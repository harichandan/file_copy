CC = gcc
CFLAGS = -Wall
TARGET = copy

all: $(TARGET)

$(TARGET): copy.o file_ops.o file_valid.o
	$(CC) $(CFLAGS) -o $(TARGET) copy.o file_ops.o file_valid.o
	rm -rf *.o

copy.o: copy.c
	$(CC) -c copy.c

file_ops.o: file_ops.c
	$(CC) -c file_ops.c

file_valid.o: file_valid.c
	$(CC) -c file_valid.c

clean:
	rm -rf *.o

cleanall:
	rm -rf $(TARGET) *.o
