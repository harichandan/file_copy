#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include "file_ops.h"
#include "file_valid.h"

#define NUM 2
#define OFFSET 64
#define BUF_LEN 64

int pos = 0;
pthread_mutex_t lock;

struct file_data {
  FILE *source;
  FILE *dest;
} thread_data;

void *write_to_file(void *data) {
  struct file_data *t_data = (struct file_data*) data;
  char *buffer = malloc(BUF_LEN);
  int eof = fgetc(t_data->source);
  while (eof != EOF) {
    pthread_mutex_lock(&lock);
    memset(buffer, 0, BUF_LEN);
    fseek(t_data->source, pos, SEEK_SET);
    fread(buffer, BUF_LEN, 1, t_data->source);
    fseek(t_data->dest, pos, SEEK_SET);
    fputs(buffer, t_data->dest);
    pos += BUF_LEN;
    eof = fgetc(t_data->source);
    pthread_mutex_unlock(&lock);
  }
  free(buffer);
  pthread_exit(NULL);
}

int main(int argc, char **argv) {
  if(argc < 3) {
    fprintf(stderr, "Error in syntax: copy src_path dest_path\n");
    exit(1);
  }

  pthread_t read_threads[NUM];
  check_path(argv[1]);

  pthread_mutex_init(&lock, NULL);

  FILE *source = file_open(argv[1], "r");
  FILE *dest   = file_open(argv[2], "w");
  thread_data.source = source;
  thread_data.dest = dest;

  for(int i=0; i<NUM; i++) {
    pthread_create(&read_threads[i], NULL, write_to_file, (void*) &thread_data);
  }

  for(int i=0; i<NUM; i++)
    pthread_join(read_threads[i], NULL);

  file_close(source);
  file_close(dest);
  return 0;
}
