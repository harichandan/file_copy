#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

FILE* file_open(const char *filename, const char *mode) {
  FILE* file = fopen(filename, mode);
  if(file == NULL) {
    fprintf(stderr, "Could not open %s\n", filename);
    exit(1);
  }
  return file;
}

int file_close(FILE* stream) {
  int close = fclose(stream);
  if(close) {
    printf("%s\n", strerror(errno));
    return -1;
  }
  return 0;
}
