FILE* file_open(const char *filename, const char *mode);
int file_close(FILE *stream);
void* file_read();
