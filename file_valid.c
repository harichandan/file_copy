#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

void check_path(char *path) {
  if(access(path, F_OK)==-1) {
    fprintf(stderr, "%s is not a valid path\n", path);
    exit(1);
  }
}
